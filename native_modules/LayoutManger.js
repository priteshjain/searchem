import {Module} from 'react-360-web';

const r360 = new ReactInstance(
  'MyApp.bundle?platform=vr',
  document.getElementById('container'),
  {
    // Register custom modules at init time
    nativeModules: [
      new MyModule(),
    ]
  }
);

class LayoutManager extends Module {
  constructor() {
    super('LayoutManager'); // Makes this module available at NativeModules.MyModule
  }

  // This method will be exposed to the React app
  registerScreens(groups) {
    groups.forEach(group => {      
      const surface = r360.renderToLocation(
        r360.createRoot(group.name),
        new Location([
          group.location.x, 
          group.location.y, 
          group.location.z
        ])
      );    
    });
  }

  hideScreens(groups) {
    groups.forEach(group => {      
        r360.detachRoot(surface),
    });
}

