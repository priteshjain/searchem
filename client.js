import {ReactInstance, Location, Surface} from 'react-360-web';
// import { LayoutManger } from "./native_modules/LayoutManger";
function init(bundle, parent, options = {}) {
  const r360 = new ReactInstance(bundle, parent, {
    fullScreen: true,
    ...options,
    nativeModules: [
      // new LayoutManger()
    ]
  });

  const groupPanel = new Surface(300, 600, Surface.SurfaceShape.Flat);
  groupPanel.setAngle(0.5, 0);
  
  r360.renderToLocation(
    r360.createRoot('marker1'),
    new Location([-700, 150, -700])
  );

  r360.renderToLocation(
    r360.createRoot('marker2'),
    new Location([600, 100, -600])
  );

  r360.renderToLocation(
    r360.createRoot('marker3'),
    new Location([-600, 100, 600])
  );

  
  r360.renderToLocation(
    r360.createRoot('marker4'),
    new Location([600, 100, 600])
  );


  r360.renderToSurface(
    r360.createRoot('GroupPanel'),
    groupPanel,
    new Location([0, 1, 0])
  );

  const PersonProfilePanel = new Surface(300, 600, Surface.SurfaceShape.Flat);
  PersonProfilePanel.setAngle(-0.5, 0);
  r360.renderToSurface(
    r360.createRoot('PersonProfile'),
    PersonProfilePanel
  );
  r360.compositor.setBackground('./static_assets/360bg.jpg');
}



window.React360 = {init};


// Location
// ["left", "-down", "-front"]