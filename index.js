import * as React from "react";
import { AppRegistry, View } from "react-360";
import * as Store from "./Store";
import Marker from "./components/Marker";
import GroupPanel from './components/GroupPanel';
import PersonProfile from './components/PersonProfile';


Store.initialize();

const Maker1 = () => (
  <View >
    <Marker group={Store.State.groups[0]}></Marker>
  </View>
);

const Maker2 = () => (
  <View >
    <Marker group={Store.State.groups[1]}></Marker>
  </View>
);

const Maker3 = () => (
  <View >
    <Marker group={Store.State.groups[2]}></Marker>
  </View>
);

const Maker4 = () => (
  <View >
    <Marker group={Store.State.groups[3]}></Marker>
  </View>
);


AppRegistry.registerComponent("marker1", () => Maker1)
AppRegistry.registerComponent("marker2", () => Maker2)
AppRegistry.registerComponent("marker3", () => Maker3)
AppRegistry.registerComponent("marker4", () => Maker4)
AppRegistry.registerComponent('GroupPanel', () => GroupPanel);
AppRegistry.registerComponent('PersonProfile', () => PersonProfile);