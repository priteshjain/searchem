import * as React from "react";
/**
 * If you want to share data between multiple root components, you'll need a
 * global store like Redux. This is similar to building a web app where you
 * want to synchronize data between a sidebar and a main view - just extended
 * into three dimensions.
 * To simplify this sample, we implement a trivial Redux-like store that will
 * ensure all of our elements are synchronized.
 */
export let State = {
  people: [
    {
      id: 1,
      name: "mahanthesh",
      photo: "/static_assets/photos/people/1.jpg",
      tags: "tag1, tag2",
      description: "Hi I'm a front end developer",
      groupId: 1,
      additionalDetails: {
        contact: "7892611653",
        college: "sjbit"
      }
    },
    {
      id: 2,
      name: "Parikshith",
      photo: "/static_assets/photos/people/2.jpg",
      tags: "sparks, AR",
      description: "Hi I work on UI and UX",
      groupId: 1,
      additionalDetails: {
        contact: "7892145789",
        college: "sjbit"
      }
    },
    {
      id: 3,
      name: "Gokul nath",
      photo: "/static_assets/photos/people/3.jpg",
      tags: "sparks, AR",
      description:
        "I'm an engineering graduate with front end development experience",
      groupId: 2,
      additionalDetails: {
        college: "anna university",
        contact: "8870718168"
      }
    },
    {
      id: 4,
      name: "Gokul raj",
      photo: "/static_assets/photos/people/4.jpg",
      tags: "sparks, AR",
      description:
        "I'm an engineering graduate with front end development experience",
      groupId: 2,
      additionalDetails: {
        college: "anna university"
      }
    },
    {
      id: 5,
      name: "Vimal",
      photo: "/static_assets/photos/people/5.jpg",
      tags: "sparks, AR",
      description: "Working as a software developer in Jizo soft",
      groupId: 2,
      additionalDetails: {
        college: "anna university",
        contact: "8870718168",
        type: "java script developer"
      }
    },
    {
      id: 6,
      name: "anand",
      photo: "/static_assets/photos/people/6.jpg",
      tags: "sparks, AR",
      description: "The Sparkers",
      groupId: 4,
      additionalDetails: {}
    },
    {
      id: 7,
      name: "rakshit",
      photo: "/static_assets/photos/people/7.jpg",
      tags: "sparks, AR",
      description: " The Sparkers",
      groupId: 4,
      additionalDetails: {}
    },
    {
      id: 8,
      name: "Balasundar",
      photo: "/static_assets/photos/people/8.jpg",
      tags: "HTML Games",
      description: "I'm from sivakasi in Tamil nadu",
      groupId: 6,
      additionalDetails: {
        company: "fame technologies",
        type: "back end developer"
      }
    },
    {
      id: 9,
      name: "kennedy fransis",
      photo: "/static_assets/photos/people/9.jpg",
      tags: "HTML Games",
      description: "We're developing a game using unity and HTML",
      groupId: 6,
      additionalDetails: {
        college: "oxford college of engineering",
        contact: "8217681757"
      }
    },
    {
      id: 10,
      name: "guru",
      photo: "/static_assets/photos/people/10.jpg",
      tags: "spurt, AR",
      description: "He's the best API maker",
      groupId: 6,
      additionalDetails: {
        college: "oxford college of engineering"
      }
    }
  ],
  groups: [
    {
      id: 1,
      name: "Bite Code",
      photo: "/static_assets/photos/groups/1.jpg"
    },
    {
      id: 2,
      name: "Blitez Scalars",
      photo: "/static_assets/photos/groups/2.jpg"
    },
    {
      id: 4,
      name: "Archimedes",
      photo: "/static_assets/photos/groups/3.jpg"
    },
    {
      id: 6,
      name: "Phantom Siege",
      photo: "/static_assets/photos/groups/4.jpg"
    }
  ],
  currentPerson: -1,
  currentGroupId: -1,
  currentSearchTerm: "",
  searchResults: {}
};

const listeners = new Set();

function updateComponents() {
  for (const cb of listeners.values()) {
    cb();
  }
}

export function initialize() {
  // Read Persons and groups from data
  // Object.keys(data).forEach(key => {
  // });
  updateComponents();
  return State;
}

export function setCurrentPerson(id) {
  State.currentPerson = id;
  State.currentGroupId = State.people.find((person) => person.id == id).groupId;
  console.log(State)
  updateComponents();
}

export function setCurrentGroupId(id) {
  State.currentGroupId = id;
  State.currentPerson = -1;
  updateComponents();
}

export function search(searchTerm) {
  results = {
    people: [],
    groups: []
  };

  results.people = searchPeople(searchTerm);
  results.groups = searchGroups(searchTerm);
  State.searchResults = results;
  updateComponents();
}

function searchPeople(searchTerm) {
  return State.people.filter(person => person.name.includes(searchTerm));
}

function searchGroups(searchTerm) {
  return State.groups.filter(group => group.name.includes(searchTerm));
}

export function connect(Component) {
  return class Wrapper extends React.Component {
    state = {
      people: State.people,
      groups: State.groups,
      currentPerson: State.currentPerson,
      currentGroupId: State.currentGroupId,
      currentSearchTerm: State.currentSearchTerm,
      searchResults: State.searchResults
    };

    _listener = key => {
      this.setState({
        people: State.people,
        groups: State.groups,
        currentPerson: State.currentPerson,
        currentGroupId: State.currentGroupId,
        currentSearchTerm: State.currentSearchTerm,
        searchResults: State.searchResults
      });
    };

    componentDidMount() {
      listeners.add(this._listener);
    }

    componentWillUnMount() {
      listeners.delete(this._listener);
    }

    render() {
      return (
        <Component
          {...this.props}
          people={this.state.people}
          groups={this.state.groups}
          currentPerson={this.state.currentPerson}
          currentGroupId={this.state.currentGroupId}
          currentSearchTerm={this.state.currentSearchTerm}
          searchResults={this.state.searchResults}
        />
      );
    }
  };
}
