# searchem ( pronounced as `Search them`)

## Team members
[Santosh S](https://linkedin.com/in/santosh-selvasundar/ "Santosh S")
[Chirag B](https://twitter.com/chirag38986106/ "Chirag B")
[Aharshraj G](https://www.linkedin.com/in/aharshraj-gunasekaran-0156aa129 "Aharshraj G")
[Pritesh Jain](https://twitter.com/sshpritesh/ "Pritesh Jain")


## Inspiration
Joining a large organization, usually makes you feel isolated for first few months. One of our team members recently went through this pain. He was discussing about this with us. This reminded us about earlier experiences which we had during our inductions. That's why, We thought of solving this problem creatively. 

## What it does
Its a Web Platform enabled with VR that allows employees to access the platform anywhere easily, find colleagues and collaborate with them interactively. 

## How we built it
It is built with React 360 as the Base Platform for VR. We Collated sample data from organisation where the Hackathon was held. The React 360 application, is enabled on Web, Android as well as IOS Platforms.

## Challenges we ran into
1. Teaming Up
2. Defining boundaries for prototype that can be ideated and built within the Hackathon timeline.
3. Working on relatively new technology without react 360 technical mentors.
4. Working without red bull was very difficult overnight. :D

## Accomplishments that we're proud of
1. Teaming unknown people and collaborating meaningfully to achieve the prototype.
2. We built an demo able react 360 application within 24 hours.
3. Actually working on a project that impacts our daily lives as an employee

## What we learned
1. React philosophy of learn once write everywhere works perfectly.
2. Product oriented thinking process.
3. Some members worked on react for first time and still managed to pull of a demo able project.
4. Collaboration with participants whom we have never met earlier.
5. WE CAN MAKE IT HAPPEN.

## What's next for SearchEm
1. Researching about the possibility how it can be sold as an Add on for existing HR / People management software that allow the employees to find and collaborate in meaningful ways.
2. Explore integration with linked in and find possible revenue generation probabilities for this feature.


