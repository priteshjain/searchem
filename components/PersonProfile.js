import * as React from 'react';
import {Image, StyleSheet, Text, View, VrButton} from 'react-360';
import {connect, setCurrent} from '../Store';

const PersonProfile = props => {

  renderProfilePanel = () => {
    const currentPerson = props.people.find((person) => person.id == props.currentPerson)
    return (
      <View style={styles.wrapper}>
        <Image style={styles.cardImage} source={{uri: currentPerson.photo}} />
        <View style={styles.cardDetailsContainer}>
          <View style={styles.DetailsRow}>
            <Text  style={styles.DetailsKey}>Name</Text>
            <Text style={styles.DetailsValue}>: {currentPerson.name}</Text>
          </View>
          {Object.entries(currentPerson.additionalDetails).map(([key,value]) => (
            <View key={key} style={styles.DetailsRow}>
              <Text  style={styles.DetailsKey}>{key}</Text>
              <Text style={styles.DetailsValue}>: {value}</Text>
            </View>
          ))}
          <View style={styles.DetailsRow}>
            <Text  style={styles.DetailsKey}>About Me</Text>
            <Text style={styles.DetailsValue}>: {currentPerson.description}</Text>
          </View>
        </View>
    </View>
    )
  }

  renderEmpty = () => {
    return (
      <View></View>
    )
  }

  renderProfile = () => {
    if (props.currentPerson == -1) {
      return renderEmpty()
    }
    else {
      return renderProfilePanel()
    }
  }

  return (    
    <View>{renderProfile()}</View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    width: 300,
    height: 600,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    borderColor: '#e17055',
    borderWidth: 2,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    padding:10,
    color:'#e17055'
  },
  cardImage: {
    width: '100%',
    height: '45%'
  },
  cardDetailsContainer: {
    width: '100%',
    height: '55%',
    display: 'flex',
    flexDirection : 'column'
  },
  DetailsRow: {
    padding: '5 20 5 20',
    display: 'flex',
    flexDirection : 'row'
  },
  DetailsKey: {
    flex:1,
    color:'#e17055',
    fontWeight: 'bold'
  },
  DetailsValue: {
    flex:1,
    color:'#e17055',
    fontWeight: 'bold'
  }
});

const ConnectedPersonProfile = connect(PersonProfile);

export default ConnectedPersonProfile;