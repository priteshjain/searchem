import * as React from "react";
import { StyleSheet, Text, View, VrButton, Image } from "react-360";
import { connect } from "../Store";
import GroupPhoto from "./GroupPhoto";
import GroupMember from "./GroupMember";

const GroupPanel = props => {

  renderGroupPanel = () => {
    let groupId = props.currentGroupId,
      people = props.people.filter(person => person.groupId == groupId);
    const showMembers = people => {
      return people.map((element, index) => (
        <GroupMember key={index} person={element}/>
      ));
    };
    return (
      <View style={styles.wrapper}>
        <GroupPhoto id={groupId} />
        <View style={styles.memberGroupWrapper}>{showMembers(people)}</View>
      </View>
    );
  };

  renderEmpty = () => {
    return <View></View>;
  };

  renderGroup = () => {
    if (props.currentGroupId != -1) {
      return this.renderGroupPanel();
    } else {
      return this.renderEmpty();
    }
  };

    return (    
      <View>{renderGroup()}</View>
    )
  // render() {
  //   return <View>{renderGroupPanel()}</View>;
  // }
}

const styles = StyleSheet.create({
  wrapper: {
    width: 300,
    height: 600,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    borderColor: "#e17055",
    borderWidth: 2,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "stretch",
    padding: 10
  },
  memberGroupWrapper: {
    width: "100%",
    height: "50%",
    display: "flex",
    flexWrap: "wrap",
    alignItems: "stretch",
    flexDirection: "row"
  }
});

const ConnectedGroupPanel = connect(GroupPanel);

export default ConnectedGroupPanel;
