import React, { Component } from "react";
import { StyleSheet, Text, View, Image, VrButton } from "react-360";
import { connect, setCurrentPerson } from "../Store";

class GroupMember extends Component {
  setCurrentPerson = () => {
    setCurrentPerson(this.props.person.id);
  };

  render() {
    return (
      <VrButton
        style={styles.personGroup}
        onClick={() => this.setCurrentPerson()}
      >
        <Image style={styles.cardImage} source={{ uri: this.props.person.photo }} />
        <Text style={styles.cardName}>{this.props.person.name}</Text>
      </VrButton>
    );
  }
}

const styles = StyleSheet.create({
  personGroup: {
    width: "50%",
    height: "50%",
    padding:'2%'
  },
  cardImage: {
    width: "96%",
    height: "76%"
  },
  cardName: {
    textAlign: "center",
    color:'#e17055',
    fontWeight: 'bold'
  }
});

const ConnectedGroupMember = connect(GroupMember);

export default ConnectedGroupMember;
