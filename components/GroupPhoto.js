import React, { Component } from 'react'
import {StyleSheet, Text, View, Image} from 'react-360';
import {connect} from '../Store';

class GroupPhoto extends Component {
    render() {
        var currentGroup = this.props.groups.find(x=>x.id==this.props.id)
        return (
            <View style={styles.groupCard}>
                <Image style={styles.cardImage} source={{uri: currentGroup.photo}} />
                <View style={styles.cardDetailsContainer}>
                    <View style={styles.DetailsRow}>
                        <Text  style={styles.DetailsKey}>Group Name</Text>
                        <Text style={styles.DetailsValue}>: {currentGroup.name}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    groupCard:{
        width: '100%',
        height: '50%'
    },
    cardImage: {
        width: '100%',
        height: '80%'
    },
    cardDetailsContainer: {
        width: '100%',
        height: '20%',
        display: 'flex',
        flexDirection : 'column'
    },
    DetailsRow: {
        padding: '5px 20px 5px 20px',
        display: 'flex',
        flexDirection : 'row'
    },
    DetailsKey: {
        flex:1,
        color:'#e17055',
        fontWeight: 'bold'
    },
    DetailsValue: {
        flex:1,
        color:'#e17055',
        fontWeight: 'bold'
    }
});
    
const ConnectedGroupPhoto = connect(GroupPhoto);

export default ConnectedGroupPhoto;
