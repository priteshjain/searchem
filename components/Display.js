import React, { Component } from 'react'
import {StyleSheet, Text, View, VrButton, Image} from 'react-360';

class Display extends Component {
    render() {
        return (
            <View>
            <Image style={styles.smallImage} source={{uri: this.props.photo}} />
            <Text>{this.props.name}</Text>
        </View>

        )
    }
}

const styles = StyleSheet.create({
    smallImage : {
        width : 50,
        height : 50
    }
})


export default Display
