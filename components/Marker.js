import * as React from "react";
import { StyleSheet, Text, View, VrButton, Image, Animated } from "react-360";
import { connect, setCurrentGroupId } from "../Store";

class Marker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rotation: 0,
      bounceValue: new Animated.Value(1)
    };
    
    this.lastUpdate = Date.now();
    this.rotate = this.rotate.bind(this);

  }

  componentDidMount() {
    this.rotate();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.group !== nextProps.group) {
      // animate the planet
      const groupConfig = {
        value: this.state.bounceValue,
        initial: 0.3,
        toValue: 1,
        friction: 5
      };

      this.bounce(groupConfig);
    }
  }

  setCurrentGroup = () => {
    console.log("setCurrentGroupId")
    setCurrentGroupId(this.props.group.id)
  }

    // planet rotate animation
    rotate() {
      const now = Date.now();
      const delta = now - this.lastUpdate;
  
      this.time++;
  
      this.lastUpdate = now;
      this.setState({
        rotation: this.state.rotation + delta / 120
      });
      this.frameHandle = requestAnimationFrame(this.rotate);
    }
  
    // bounce animation
    bounce({value, initial, toValue, friction = 1.5}) {
      value.setValue(initial);

      Animated.spring(
        value,
        {
          toValue,
          friction,
        }
      ).start();
    }
    // cancel the rotate animation
    componentWillUnmount() {
      if (this.frameHandle) {
        cancelAnimationFrame(this.frameHandle);
        this.frameHandle = null;
      }
    }

  render() {
    const {rotation} = this.state;
    const scale = this.state.bounceValue;
    return (
      // <View style={styles.planet}>
      <Animated.View style={{transform: [{scale}]}}>
      <VrButton
        style={{
          height: 150,
          width: 150,
          position: 'absolute',
          backgroundColor: "#000000",
          overflow: "hidden",
          transform: [
            {translate: [0, 0, 0]},
            {scale: 1},
            {rotateY: rotation}
          ]
        }}        
        onClick={() => this.setCurrentGroup(this.props.group.id)}>
        <Image style={styles.postButtonPreview} source={{uri: this.props.group.photo}} />
        <View style={styles.postButtonLabel}>
          <Text style={styles.postButtonName}>{this.props.group.name}</Text>
        </View>
      </VrButton>
      </Animated.View>
      // </View>
    );
  }
}

const styles = StyleSheet.create({  
  postButtonInfo: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "rgba(0, 0, 0, 0.2)",
    flexDirection: "column"
  },
  postButtonPreview: {
    width: 150,
    height: 150,
  },
  postButtonInfoHover: {
    backgroundColor: "rgba(0, 0, 0, 0)"
  },
  postButtonLabel: {
    backgroundColor: "rgba(0, 0, 0, 0.8)",
    paddingHorizontal: 10,
    paddingVertical: 2,
    alignSelf: "flex-start",
    width:'100%'
  },
  postButtonName: {
    fontSize: 24,
    width:'100%'
  },
  postButtonAuthor: {
    fontSize: 16
  }
});

const connectedMarker = connect(Marker)

export default connectedMarker;
