import React from "react";
import {
  AppRegistry,
  StyleSheet,
  View,
  VrButton,
  NativeModules
} from "react-360";
import { EmojiText } from "react-360-keyboard";
import { connect } from "../Store";

class SearchBox extends React.Component {
  state = {
    searchTerm: null
  };
  onClick = () => {
    NativeModules.Keyboard.startInput({
      initialValue: this.state.searchTerm,
      placeholder: "Search  Here",
      emoji: true
    }).then(searchTerm => {
      console.log(searchTerm);
      this.setState({ searchTerm });
    });
  };
  render() {
    return (
      <VrButton style={styles.greetingBox} onClick={this.onClick}>
        <EmojiText style={styles.greeting}>
          {this.state.searchTerm || "Click to search members"}
        </EmojiText>
      </VrButton>
    );
  }
}

const styles = StyleSheet.create({
  greetingBox: {
    width: 300,
    height: 600,
    backgroundColor: 'rgba(0, 0, 0, 1)',
    borderColor: '#303050',
    borderWidth: 2,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },
  greeting: {
    fontSize: 30
  }
});

const ConnectedSearchBox = connect(SearchBox)
export default ConnectedSearchBox
